<?php

class HomeController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/administrator/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view'),
'users'=>array('*'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete'),
'users'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Home;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Home']))
		{
			$model->attributes=$_POST['Home'];
			$simpan=CUploadedFile::getInstance($model,'ilustrasi1');
			$simpan2=CUploadedFile::getInstance($model,'ilustrasi2');
			$simpan3=CUploadedFile::getInstance($model,'ilustrasi3');
			
				if($simpan!==null)
				$model->ilustrasi1 = str_replace(' ','-',time()."_".$simpan->name);

				if($simpan2!==null)
				$model->ilustrasi2 = str_replace(' ','-',time()."_".$simpan2->name);

				if($simpan3!==null)
				$model->ilustrasi3 = str_replace(' ','-',time()."_".$simpan3->name);
				
				if($model->save())
				{
					if($simpan!==null)
					$simpan->saveAs(Yii::app()->basePath .'/../images/home/'.$model->ilustrasi1);

					if($simpan2!==null)
					$simpan->saveAs(Yii::app()->basePath .'/../images/home/'.$model->ilustrasi2);

					if($simpan3!==null)
					$simpan->saveAs(Yii::app()->basePath .'/../images/home/'.$model->ilustrasi3);
					
					$this->redirect(array('view','id'=>$model->id));
				
				}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
        $model=$this->loadModel($id);
        $oldFile = $model->ilustrasi1;
        $oldFile2 = $model->ilustrasi2;
        $oldFile3 = $model->ilustrasi3;
        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        
        if(isset($_POST['Home']))
        {
            
            $model->attributes=$_POST['Home'];
            //gambar
            $gambar = CUploadedFile::getInstance($model,'ilustrasi1');
            $gambar2 = CUploadedFile::getInstance($model,'ilustrasi2');
            $gambar3 = CUploadedFile::getInstance($model,'ilustrasi3');
            
            if($gambar!==null)
            {
                    $model->ilustrasi1 = str_replace(' ','-',time().'_'.$gambar->name);
            }else {
                    $model->ilustrasi1 = $oldFile;
            }

            if($gambar2!==null)
            {
                    $model->ilustrasi2 = str_replace(' ','-',time().'_'.$gambar2->name);
            }else {
                    $model->ilustrasi2 = $oldFile2;
            }

            if($gambar3!==null)
            {
                    $model->ilustrasi3 = str_replace(' ','-',time().'_'.$gambar3->name);
            }else {
                    $model->ilustrasi3 = $oldFile2;
            }
            
            if($model->save())
            {
                if($gambar !== null)
				{
                        $path = Yii::app()->basePath.'/../images/home/';
                        $gambar->saveAs($path.$model->ilustrasi1);
                        
                        if(file_exists($path.$oldFile))
                                unlink($path.$oldFile);

                }

                if($gambar2 !== null)
				{
                        $path = Yii::app()->basePath.'/../images/home/';
                        $gambar2->saveAs($path.$model->ilustrasi2);
                        
                        if(file_exists($path.$oldFile2))
                                unlink($path.$oldFile2);

                }

                if($gambar3 !== null)
				{
                        $path = Yii::app()->basePath.'/../images/home/';
                        $gambar3->saveAs($path.$model->ilustrasi3);
                        
                        if(file_exists($path.$oldFile3))
                                unlink($path.$oldFile3);

                }
                
                Yii::app()->user->setFlash('success','Foto berhasil diperbahrui');
                $this->redirect(array('view','id'=>$model->id));
            }
        }
        
        $this->render('update',array(
            'model'=>$model,
        ));
    }

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Home');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Home('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Home']))
$model->attributes=$_GET['Home'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Home::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='home-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
