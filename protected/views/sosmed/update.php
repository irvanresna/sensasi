<?php
$this->breadcrumbs=array(
	'Sosmeds'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Sosmed','url'=>array('index')),
	array('label'=>'Create Sosmed','url'=>array('create')),
	array('label'=>'View Sosmed','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Sosmed','url'=>array('admin')),
	);
	?>

	<h1>Update Sosmed <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>