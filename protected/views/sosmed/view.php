<?php
$this->breadcrumbs=array(
	'Sosmeds'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Sosmed','url'=>array('index')),
array('label'=>'Create Sosmed','url'=>array('create')),
array('label'=>'Update Sosmed','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Sosmed','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Sosmed','url'=>array('admin')),
);
?>

<h1>View Sosmed #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'twitter',
		'facebook',
),
)); ?>
