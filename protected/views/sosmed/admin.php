<?php
$this->breadcrumbs=array(
	'Sosmeds'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Sosmed','url'=>array('index')),
array('label'=>'Create Sosmed','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('sosmed-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Sosmeds</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'sosmed-grid',
'type'=>'bordered hover striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		),
		'twitter',
		'facebook',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
