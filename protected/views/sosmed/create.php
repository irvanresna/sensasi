<?php
$this->breadcrumbs=array(
	'Sosmeds'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Sosmed','url'=>array('index')),
array('label'=>'Manage Sosmed','url'=>array('admin')),
);
?>

<h1>Create Sosmed</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>