<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'about-us-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->ckEditorRow($model,'about_us',array('options' => array('fullpage' => 'js:true',
										  'width' => '640',
										  'height'=>'300',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?><br><br>

	<?php echo $form->ckEditorRow($model,'visi',array('options' => array('fullpage' => 'js:true',
										  'width' => '640',
										  'height'=>'300',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?><br><br>

	<?php echo $form->ckEditorRow($model,'misi',array('options' => array('fullpage' => 'js:true',
										  'width' => '640',
										  'height'=>'300',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?><br><br>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
