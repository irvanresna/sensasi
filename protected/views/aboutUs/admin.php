<?php
$this->breadcrumbs=array(
	'About Uses'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List AboutUs','url'=>array('index')),
array('label'=>'Create AboutUs','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('about-us-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage About Us</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'about-us-grid',
'type'=>'hover striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		),
		'about_us',
		'visi',
		'misi',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
