<?php
$this->breadcrumbs=array(
	'Homes',
);

$this->menu=array(
array('label'=>'Create Home','url'=>array('create')),
array('label'=>'Manage Home','url'=>array('admin')),
);
?>

<h1>Homes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
