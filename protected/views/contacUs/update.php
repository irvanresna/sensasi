<?php
$this->breadcrumbs=array(
	'Contac Uses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List ContacUs','url'=>array('index')),
	array('label'=>'Create ContacUs','url'=>array('create')),
	array('label'=>'View ContacUs','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ContacUs','url'=>array('admin')),
	);
	?>

	<h1>Update ContacUs <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>