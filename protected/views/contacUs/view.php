<?php
$this->breadcrumbs=array(
	'Contac Uses'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List ContacUs','url'=>array('index')),
array('label'=>'Create ContacUs','url'=>array('create')),
array('label'=>'Update ContacUs','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ContacUs','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ContacUs','url'=>array('admin')),
);
?>

<h1>View ContacUs #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'alamat',
		'no_hp',
		'no_telp',
		'email',
		'website',
),
)); ?>
