<?php
$this->breadcrumbs=array(
	'Sliders'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Slider','url'=>array('index')),
array('label'=>'Create Slider','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('slider-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Sliders</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'slider-grid',
'type'=>'hover striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		),
		array(
		      'name'=>'file',
		      'type'=>'raw',
		      'value'=>'"<img width=40% src=".Yii::app()->baseUrl."/images/slider/".$data->file.">"',
		      'htmlOptions'=>array(
				'style'=>'text-align:center',
		      ),
		),
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
