<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'slider-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php if($model->file !=null){?>
		<img width="50%" src="<?php echo Yii::app()->theme->baseUrl.'/images/slider/'.$model->file;?>"><br><br>
	<?php }?>
	<?php echo $form->fileFieldRow($model,'file',array('class'=>'span5','maxlength'=>225)); ?><br><br>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
