<?php

/**
 * This is the model class for table "home".
 *
 * The followings are the available columns in table 'home':
 * @property integer $id
 * @property string $content1
 * @property string $ilustrasi1
 * @property string $content2
 * @property string $ilustrasi2
 * @property string $content3
 * @property string $ilustrasi3
 */
class Home extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'home';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ilustrasi1, ilustrasi2, ilustrasi3', 'length', 'max'=>255),
			array('content1, content2, content3', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, content1, ilustrasi1, content2, ilustrasi2, content3, ilustrasi3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'content1' => 'Content1',
			'ilustrasi1' => 'Ilustrasi1',
			'content2' => 'Content2',
			'ilustrasi2' => 'Ilustrasi2',
			'content3' => 'Content3',
			'ilustrasi3' => 'Ilustrasi3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('content1',$this->content1,true);
		$criteria->compare('ilustrasi1',$this->ilustrasi1,true);
		$criteria->compare('content2',$this->content2,true);
		$criteria->compare('ilustrasi2',$this->ilustrasi2,true);
		$criteria->compare('content3',$this->content3,true);
		$criteria->compare('ilustrasi3',$this->ilustrasi3,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Home the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
