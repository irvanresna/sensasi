<?php

	  $baseUrl = Yii::app()->theme->baseUrl; 

	  $cs = Yii::app()->getClientScript();

	  Yii::app()->clientScript->registerCoreScript('jquery');

	?>

<!-- SLIDER 

================================================== -->

<div id="ei-slider" class="ei-slider posisi">

	<ul class="ei-slider-large">

	<?php foreach(Slider::model()->findAll() as $model) { ?>

		<li>

		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/slider/<?php echo $model->file; ?>" alt="image01" class="responsiveslide">

		</li>

	<?php } ?>

	</ul>

	<!-- slider-thumbs -->

	<ul class="ei-slider-thumbs">

		<li class="ei-slider-element">Current</li>

		<?php foreach(Slider::model()->findAll() as $model) { ?>

			<li><a href="#">Slide 1</a><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/slider/<?php echo $model->file; ?>" class="slideshowthumb" alt="thumb01"/></li>

		<?php } ?>

	</ul>	

</div>

<!--<div class="minipause">

</div>-->

<!-- SUBHEADER

================================================== -->

<div id="subheader">

	<div class="row">

		<div class="twelve columns">

		</div>

	</div>

</div>



<!-- CONTENT 

================================================== -->

<div class="row">

	<div class="twelve columns">

		<div class="centersectiontitle">

			<h4>Gallery</h4>
			<hr class="line">

		</div>

	</div>

	<br>

	<br>

	<script type="text/javascript" charset="utf-8">

			$(document).ready(function(){

				$("area[rel^='prettyPhoto[gallery1]']").prettyPhoto();

				

				$(".gallery:first a[rel^='prettyPhoto[gallery1]']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: false});

				$(".gallery:gt(0) a[rel^='prettyPhoto[gallery1]']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});

		

				$("#custom_content a[rel^='prettyPhoto[gallery1]']:first").prettyPhoto({

					custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',

					changepicturecallback: function(){ initialize(); }

				});



				$("#custom_content a[rel^='prettyPhoto[gallery1]']:last").prettyPhoto({

					custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',

					changepicturecallback: function(){ _bsap.exec(); }

				});

			});

		</script>

	<div class="gallery twelve columns homepage" style="text-align : justify">

	<center>

		<?php foreach(Gallery::model()->findAll(array("order"=>"id DESC")) as $model){?>

			<li  class="four columns events"><a href="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/<?php echo $model->gambar; ?>" rel="prettyPhoto[gallery1]" title=""><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/gallery/<?php echo $model->gambar; ?>" alt="" /></a></li>

		<?php } ?>

	</center>

	</div>

</div>

<div class="hr">

</div>



