<?php

	  $baseUrl = Yii::app()->theme->baseUrl; 

	  $cs = Yii::app()->getClientScript();

	  Yii::app()->clientScript->registerCoreScript('jquery');

	?>



<!-- SUBHEADER

================================================== -->

<div id="subheader">

	<div class="row">

		<div class="twelve columns">

		</div>

	</div>

</div>



<!-- CONTENT 

================================================== -->

<div class="row contact-isi">

	<div class="twelve columns">

	</div>


	<div class="seven columns homepage" style="text-align : justify">
		<div class="centersectiontitle">

			<h4>HOW CAN WE HELP YOU</h4>
			<hr class="line">
		</div>
		<p>

			<?php if(Yii::app()->user->hasFlash('contact')): ?>



<div class="flash-success">

	<?php echo Yii::app()->user->getFlash('contact'); ?>

</div>



<?php else: ?>



<p>

If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.

</p>



<div class="form">



<?php $form=$this->beginWidget('CActiveForm', array(

	'id'=>'contact-form',

	'enableClientValidation'=>true,

	'clientOptions'=>array(

		'validateOnSubmit'=>true,

	),

)); ?>



	<p class="note">Fields with <span class="required">*</span> are required.</p>



	<?php echo $form->errorSummary($model); ?>



	<div class="row">

		<?php echo $form->labelEx($model,'name'); ?>

		<?php echo $form->textField($model,'name'); ?>

		<?php echo $form->error($model,'name'); ?>

	</div>



	<div class="row">

		<?php echo $form->labelEx($model,'email'); ?>

		<?php echo $form->textField($model,'email'); ?>

		<?php echo $form->error($model,'email'); ?>

	</div>



	<div class="row">

		<?php echo $form->labelEx($model,'subject'); ?>

		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128)); ?>

		<?php echo $form->error($model,'subject'); ?>

	</div>



	<div class="row">

		<?php echo $form->labelEx($model,'body'); ?>

		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>

		<?php echo $form->error($model,'body'); ?>

	</div>



	<?php if(CCaptcha::checkRequirements()): ?>

	<div class="row">

		<?php echo $form->labelEx($model,'verifyCode'); ?>

		<div>

		<?php $this->widget('CCaptcha'); ?>

		<?php echo $form->textField($model,'verifyCode'); ?>

		</div>

		<div class="hint">Please enter the letters as they are shown in the image above.

		<br/>Letters are not case-sensitive.</div>

		<?php echo $form->error($model,'verifyCode'); ?>

	</div>

	<?php endif; ?>



	<div class="row buttons">

		<?php echo CHtml::submitButton('Submit'); ?>

	</div>



<?php $this->endWidget(); ?>



</div><!-- form -->



<?php endif; ?>	

		</p>

	</div>
	<div class="five columns homepage">
		<div class="centersectiontitle">

			<h4 style="margin-left:55px">CONTACT US</h4>
			<hr class="line">
		</div>
		<div class="ctn">

		<?php foreach(ContacUs::model()->findAll() as $model) { ?>

		<p>

			<h5>Visit Us :</h5>

			<ul class="contact-a isi">

				<li><i class="address"></i><?php echo $model->alamat;?></li>

			</ul>

		</p>

		<p>

			<h5>Or Call Us :</h5>

			<ul class="contact-a isi">

				<li><i class="call"></i><?php echo $model->no_telp;?></li>

			</ul>

		</p>

		<p>

			<h5>Email Us :</h5>

			<ul class="contact-a isi">

				<li><i class="email"></i><?php echo $model->email;?></li>

			</ul>

		</p>

		<br>

		<?php } ?>

		</div>

		</div>

</div>

<div class="hr">

</div>



