<?php

	  $baseUrl = Yii::app()->theme->baseUrl; 

	  $cs = Yii::app()->getClientScript();

	  Yii::app()->clientScript->registerCoreScript('jquery');

	?>

<!-- SLIDER 

================================================== -->

<div id="ei-slider" class="ei-slider posisi">

	<ul class="ei-slider-large">

	<?php foreach(Slider::model()->findAll() as $model) { ?>

		<li>

		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/slider/<?php echo $model->file; ?>" alt="image01" class="responsiveslide">

		</li>

	<?php } ?>

	</ul>

		<!-- slider-thumbs -->

	<ul class="ei-slider-thumbs">

		<li class="ei-slider-element">Current</li>

		<?php foreach(Slider::model()->findAll() as $model) { ?>

			<li><a href="#">Slide 1</a><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/slider/<?php echo $model->file; ?>" class="slideshowthumb" alt="thumb01"/></li>

		<?php } ?>

	</ul>

</div>

<!--<div class="minipause">

</div>-->

<!-- SUBHEADER

================================================== -->

<div id="subheader">

	<div class="row">

		<div class="twelve columns">

		</div>

	</div>

</div>



<!-- CONTENT 

================================================== -->

<div class="row">

	<div class="twelve columns">

		<div class="centersectiontitle">
			<h4>Video</h4>
			<hr class="line">
		</div>

	</div>

	<br>

	<br>

	<div class="gallery-video twelve columns homepage" style="text-align : justify">
		<?php foreach(Video::model()->findAll(array("order"=>"id DESC")) as $model){?>

			<li  class="four columns videos"><span><?php echo $model->embed_code;?></span></li>

		<?php } ?>
	</div>

<div class="hr">

</div>



