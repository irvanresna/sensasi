<!-- FOOOTER 

================================================== -->

<div id="footer">

	<footer class="row">

	<p class="back-top floatright">

		<a href="#top"><span></span></a>

	</p>

	<div class="four columns">

		<h1>ABOUT US</h1>
		<?php foreach(AboutUs::model()->findAll() as $model) { ?>
		 <p><?php echo substr($model->about_us,0,266) ;?></p>
		<?php } ?>

	</div>

	<div class="four columns">

		<h1>GET SOCIAL</h1>
		
			<?php foreach(Sosmed::model()->findAll() as $model) { ?>
			<a href="<?php echo $model->facebook;?>"><div class="social facebook"></div></a>
			<?php } ?>
		
		

		
		
			<?php foreach(Sosmed::model()->findAll() as $model) { ?>
			<a href="<?php echo $model->twitter;?>"><div class="social twitter"></div></a>
			<?php } ?>
		
		
		<div class="social youtube">

			<a href="#"></a>

		</div>


	</div>

	<div class="four columns">

		<h1 class="newsmargin">NEWSLETTER</h1>

		<div class="row collapse newsletter floatright">

			<div class="ten mobile-three columns">

				<input type="text" class="nomargin" placeholder="Enter your e-mail address...">

			</div>

			<div class="two mobile-one columns">

				<a href="#" class="postfix button expand">GO</a>

			</div>

		</div>

	</div>

	</footer>

</div>

<div class="copyright">

	<div class="row">

		<div class="six columns">

			 &copy;<span class="small"> Copyright 2014 Digital Data Studio | <a href="<?php echo Yii::app()->controller->createUrl('/site/administrator');?>">Admin</a></span>

		</div>

		<div class="six columns">

			<span class="small floatright"> </span>

		</div>

	</div>

</div>

<!-- JAVASCRIPTS 

================================================== -->

<!-- Javascript files placed here for faster loading -->

<script src="<?php echo $baseUrl;?>/js/elasticslideshow.js"></script>

<script src="<?php echo $baseUrl;?>/js/jquery.carouFredSel-6.0.5-packed.js"></script>

<script src="<?php //echo $baseUrl;?>/js/foundation.min.js"></script>   

<script src="<?php echo $baseUrl;?>/js/jquery.easing.1.3.js"></script>

<script src="<?php echo $baseUrl;?>/js/jquery.cycle.js"></script>

<script src="<?php echo $baseUrl;?>/js/app.js"></script>

<script src="<?php echo $baseUrl;?>/js/modernizr.foundation.js"></script>

<script src="<?php echo $baseUrl;?>/js/slidepanel.js"></script>

<script src="<?php echo $baseUrl;?>/js/scrolltotop.js"></script>

<script src="<?php echo $baseUrl;?>/js/hoverIntent.js"></script>

<script src="<?php echo $baseUrl;?>/js/superfish.js"></script>

<script src="<?php echo $baseUrl;?>/js/responsivemenu.js"></script>

<script src="<?php echo $baseUrl;?>/js/jquery.tweet.js"></script>

<script src="<?php echo $baseUrl;?>/js/twitterusername.js"></script>

<script src="<?php echo $baseUrl;?>/js/jquery.prettyPhoto.js"></script>



</body>

</html>