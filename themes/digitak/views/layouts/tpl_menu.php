<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
<div class="row menu">	
		<div class="five columns">
			<div class="logo">
				<a href="<?php echo Yii::app()->controller->createUrl('/site/index');?>"><img src="<?php echo $baseUrl;?>/images/logo.png"></a>
			</div>
		</div>
		<div class="eight columns noleftmarg">		
			<nav id="nav-wrap">
			<!--<div class="sosmed">
				<div class="social facebook">
					<a href="#"></a>
				</div>
				<div class="social twitter">
					<a href="#"></a>
				</div>
				<div class="social deviantart">
					<a href="#"></a>
				</div>
				<div class="social flickr">
					<a href="#"></a>
				</div>
				<div class="social dribbble">
					<a href="#"></a>
				</div>
			</div>-->
				<ul class="nav-bar sf-menu">
				
					<li class="current">
					<a href="<?php echo Yii::app()->controller->createUrl('/site/index');?>">Home</a>
				
					</li>
					
					<li>
					<a href="<?php echo Yii::app()->controller->createUrl('/site/profile');?>">Profile</a>
					</li>
					
					<li>
					<a href="<?php echo Yii::app()->controller->createUrl('/site/event');?>">Event</a>
					</li>

					<li>
					<a href="<?php echo Yii::app()->controller->createUrl('/site/gallery');?>">Gallery</a>
					</li>

					<li>
					<a href="<?php echo Yii::app()->controller->createUrl('/site/video');?>">Video</a>
					</li>
									
										
					<li>
					<a href="<?php echo Yii::app()->controller->createUrl('/site/contact');?>">Contact</a>
					</li>
					
				</ul>
				</nav>
		</div>	
</div>
<div class="clear"></div>
<br>
<br>
<br>
<br>
<br>